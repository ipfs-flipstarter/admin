import all from "it-all"

let clientCid = null;

/**
 * 
 * @param {import('ipfs').IPFS} ipfs 
 * @returns 
 */
export async function ipfsLoadClientSite(ipfs) {
  if (!clientCid) {
    const carFile = Buffer.from(await fetch("static/client.car").then(res => res.arrayBuffer()));
    const result = await all(ipfs.dag.import((async function * () { yield carFile }())));
    clientCid = result[0].root.cid;
    return clientCid;
  }

  return clientCid;
}