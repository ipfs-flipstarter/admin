import Router, {route} from "preact-router";
import { createHashHistory } from "history";

import Navigation from "./navigation.js"
import CreatePage from "./pages/create/index.js"
import ManagePage from "./pages/manage/index.js"

import { useState } from "preact/hooks"
import createElectrum from "@ipfs-flipstarter/utils/network/electrum.js"
import { ElectrumProvider } from "./withElectrum.js";
import { IpfsProvider } from "./withIpfs";
import { registerContributionQueries } from "@ipfs-flipstarter/utils/queries/contributions.js"
import { registerBlockchainQueries } from "@ipfs-flipstarter/utils/queries/blockchain.js"
import createIpfs from "../utils/createIpfs.js"

const electrum = createElectrum(process.env.ELECTRUM_SERVERS);

import {
  QueryClient,
  QueryClientProvider,
} from 'react-query'
import { persistQueryClient } from 'react-query/es/persistQueryClient-experimental/index.js'
import { createWebStoragePersistor } from 'react-query/es/createWebStoragePersistor-experimental/index.js'


const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: Infinity,
      cacheTime: Infinity
    },
  },
})

registerContributionQueries(queryClient, { createIpfs, defaultGatewayUrl: process.env.NODE_ENV !== 'development' && process.env.DEFAULT_GATEWAY_URL });
registerBlockchainQueries(queryClient, electrum);

export default function App() {
  const [isLoading, setIsLoading] = useState(true);

  const handleRoute = async e => {
    route(e.url, true);
  };

  const localStoragePersistor = createWebStoragePersistor({storage: window.localStorage});
  
  persistQueryClient({
    queryClient,
    persistor: localStoragePersistor,
  })

  return <>
    <QueryClientProvider client={queryClient}>
      <IpfsProvider createIpfs={createIpfs}>
        <ElectrumProvider electrum={electrum}>
          <Navigation></Navigation>
          <section class="w-full">
            <div class="p-2 xs:p-5">
              { isLoading ? <div id="load-indicator-container" class="cover-area" style="background:white;z-index: 1;">
                <ul class="load-indicator animated -half-second">
                  <li class="load-indicator__bubble"></li>
                  <li class="load-indicator__bubble"></li>
                  <li class="load-indicator__bubble"></li>
                </ul>
              </div> : <></> }
              <div className={`${isLoading ? "hidden" : ""} min-h-screen justify-content-center shadow-sm relative xs:p-8 xs:border rounded`}>
                <div id="main">
                  <div>
                    <Router history={createHashHistory()} onChange={handleRoute}>
                      <CreatePage default path="/create" setIsLoading={setIsLoading} isLoading={isLoading} />
                      <ManagePage path="/manage" setIsLoading={setIsLoading} isLoading={isLoading} />
                      <CreatePage path="/manage/:id" readOnly={true} setIsLoading={setIsLoading} isLoading={isLoading}/>
                    </Router>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </ElectrumProvider>
      </IpfsProvider>
    </QueryClientProvider>
  </>
}