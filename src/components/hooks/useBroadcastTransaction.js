import { useMutation } from 'react-query'
import { useElectrum } from "../withElectrum.js";
import { broadcastTransaction } from '@ipfs-flipstarter/utils/utils/electrum.js';

export default function useBroadcastTransaction() {
  const electrum = useElectrum();

  const broadcastTransactionMutation = useMutation((txHex) => {
    return broadcastTransaction(electrum, txHex);
  }, {
    onMutate: () => {
    },
    onError: () => {
    },
    onSettled: () => {
    },
    onSuccess: () => {
    }
  });

  return broadcastTransactionMutation.mutateAsync;
}