import { useQuery } from "react-query";
import { contributionQueryKeys } from "@ipfs-flipstarter/utils/queries/contributions.js";

export const useContributionsQuery = (recipients) => {
  return useQuery(contributionQueryKeys.list(recipients), {
    staleTime: 30 * 1000
  });
}

export default useContributionsQuery