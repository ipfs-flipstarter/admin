import {route} from "preact-router"
import {forwardRef} from "preact/compat"
import { useState, useEffect, useMemo, useCallback, useRef } from "preact/hooks"
import SimpleMDE from "react-simplemde-editor";
import { useForm, useFieldArray, useWatch } from "react-hook-form";
import bchaddr from 'bchaddrjs'
import CurrencyInput from "@sahidmiller/react-currency-input-field"
import { get, set } from 'idb-keyval';
import { ipfsLoadClientSite } from "../../../utils/ipfsLoadClientSite.js";
import createFlipstarterCampaignBlobUrl from "../../../utils/createFlipstarterCampaignBlobUrl.js";
import campaignStore from "../../../utils/campaignStore.js";
import LoadingIndicator from "../../loading-indicator.js";
import styled from "styled-components"
import FloatingLabel from "../../floating-label.js";
// import parseMarkdown from "../../../utils/markdown.js";

import { useIpfs } from "../../withIpfs.js";

const emptyRecipient = { name: "", bch: "", address: "" };
const emptyPreloadNode = { url: "", multiaddr: "" }
const today = new Date();
const todayFormatted = today.toISOString().split("T")[0];

export default function CreatePage({ readOnly, id, setIsLoading, isLoading }) {
  const { ipfs, ipfsError } = useIpfs();
  const [showAdvanced, setShowAdvanced] = useState(false);
  const [campaignData, setCampaignData] = useState(null);
  const [isSuccess, setSuccess] = useState(false);
  const [isPublishing, setIsPublishing] = useState(false);
  
  useEffect(() => {
    setTimeout(() => {
      setSuccess(() => {
        return false;
      })
    }, 4000);
  }, [isSuccess]);

  const { 
    control, 
    register, 
    handleSubmit, 
    watch, 
    formState: { errors:_errors }, 
    setValue,
    getValues,
    reset
  } = useForm({ 
    mode: "onBlur", 
    reValidateMode: 'onChange',
  });

  const errors = readOnly ? {} : _errors;

  const description = useWatch({ name: 'description', control });

  const { fields:recipients, append:addRecipient, remove:removeRecipient, update:updateRecipient} = useFieldArray({
    control,
    name: "recipients", // unique name for your Field Array
  });
  
  const { fields:preloadNodes, append:addPreloadNode, remove:removePreloadNode, update:updatePreloadNode} = useFieldArray({
    control,
    name: "preloadNodes", // unique name for your Field Array
  });
  
  const onSubmit = async (campaignData) => {
    console.log(campaignData);

    if (process.env.NODE_ENV !== 'development') {
      if (!readOnly) {
        set("draft", undefined);
      }
    }

    setIsPublishing(true);

    try {

      //Following makes it not exactly reproducing previously created data.
      const { create:createFlipstarterCampaignSite } = (await import(/* webpackChunkName: 'createFlipstarter'*/'@ipfs-flipstarter/utils/ipfs/index.js'));
      const clientCid = await ipfsLoadClientSite(ipfs);
      const clientDag = (await ipfs.dag.get(clientCid)).value;
      
      const campaign = {
        title: campaignData.title,
        image: campaignData.image,
        description: campaignData.description,
        recipients: campaignData.recipients.map((recipient) => {
          const bch = parseFloat(recipient.bch);
          const sats = Math.round(bch * SATS_PER_BCH);

          return {
            name: recipient.name,
            address: recipient.address,
            satoshis: sats,
          }
        }),
        clientConfig: {
          cid: clientCid.toString(),
          ipns: process.env.FLIPSTARTER_CLIENT_IPNS,
          record: process.env.FLIPSTARTER_CLIENT_RECORD
        }
      }

      if (campaignData.expires) {
        campaign.expires = Math.floor(new Date(campaignData.expires + "T23:59:59.999Z").getTime() / 1000)
      }

      //Connect to preload nodes before pushing to IPFS
      await ipfs.connectToPreloadNodes(campaignData.preloadNodes);
      

      const campaignCid = await createFlipstarterCampaignSite(ipfs, clientDag, campaign);
      const carUrl = await createFlipstarterCampaignBlobUrl(ipfs, campaignCid);
      const hashString = campaignCid.toString();
      const successfulPreloadNodeIndexes = await ipfs.requestPreloading(hashString, campaignData.preloadNodes, 10000);
      const gatewayUrl = campaignData.defaultGatewayUrl || process.env.DEFAULT_GATEWAY_URL;
      
      const result = {
        hash: campaignCid.toString(),
        url: gatewayUrl.replace(/\/$/, '')+ "/ipfs/" + hashString,
        download: carUrl,
        successfulPreloadNodeIndexes,
      };

      await set("draft", undefined);
      await set(result.hash, { ...campaignData, ...campaign, ...result, id: result.hash }, campaignStore);
      
      setSuccess(true);

      //Change url immediately since no longer creating
      setTimeout(() => route("/manage/" + result.hash), 400);
    
    } finally {
      
      setIsPublishing(false);
    }
  }
  const [codeMirror, setCodeMirror] = useState(null);

  //Run afterwards when other react-form-hooks have run
  useEffect(async () => {
    
    let emptyDraft = {
      title: "",
      expires: "",
      image: "",
      description: "",
      defaultGatewayUrl: process.env.DEFAULT_GATEWAY_URL,
      recipients: [emptyRecipient],
      preloadNodes: process.env.PRELOAD_NODES
    };

    setIsLoading(true);

    try {
      
      if (id) {
        const campaignData = await get(id, campaignStore);
        
        reset(Object.assign(campaignData, { 
          expires: campaignData.expires && new Date(campaignData.expires * 1000).toISOString().split('T')[0],
          recipients: campaignData.recipients.map(r => ({
            ...r,
            bch: (r.satoshis / SATS_PER_BCH).toFixed(8),
          }))
        }));
        setCampaignData(campaignData);
      
      } else {

        const draft = await get("draft");
        reset(draft || emptyDraft);
        setCampaignData(null);
      }

    } catch(err) {
      reset(emptyDraft);
    }

    const timeout = setTimeout(() => {
      setIsLoading(false);
    }, 500);

    return () => clearTimeout(timeout);

  }, [readOnly, id]);

  useEffect(() => {
    if (codeMirror) { 
      setTimeout(() => codeMirror.refresh(), 100);
    }
  }, [campaignData, codeMirror]);
  
  useEffect(() => {

    if (readOnly) return;

    let timeout;
    const subscription = watch((data, { type }) => {
      if (type === "preloadNodes" && campaignData) {
        setCampaignData({ ...campaignData, successfulPreloadNodeIndexes: [] })
      }

      //Only run once in 1s
      clearTimeout(timeout);
      timeout = setTimeout(async () => {
        await set("draft", data);
      }, 500);
    });

    return () => { subscription.unsubscribe(); }
  }, [readOnly, id]);

  const { onBlur:onDefaultGatewayBlur, ...defaultGatewayProps } = register(`defaultGatewayUrl`, { required: true, validate: validateURL  });
  const { onChange:onDescriptionChange, ...descriptionProps } = register("description", { required: true });
  
  function _onDefaultGatewayBlur(e) {
    if (!e.target.value) e.target.value = "https://dweb.link";
    return onDefaultGatewayBlur(e);
  }
  
  const checkKeyDown = (e) => {
    //Prevent form submit on Enter
    const keyCode = e.keyCode ? e.keyCode : e.which; 
    if (keyCode === 13) e.preventDefault();
  }

  const getCmInstanceCallback = useCallback((editor) => {
    setCodeMirror(editor);
  }, []);  


  useEffect(() => {
    if (codeMirror) {
      codeMirror.setOption('readOnly', readOnly);
    }
  }, [codeMirror]);

  const previewRef = useRef("");
  const mdeOptions = useMemo(() => {
    const options = {
      autoRefresh: 300, 
      previewClass: "editor-preview markdown-body prose max-w-none prose-code:text-gray-700",
      // previewRender: (text, previewElem) => {
      //   const oldPreview = previewRef.current;
      //   parseMarkdown(text).then((newPreview) => {
      //     previewRef.current = newPreview
      //     previewElem.innerHTML = newPreview;
      //   });
      //   return oldPreview;
      // }
    }
    if (readOnly) options.toolbar = false;

    return options;
  }, [readOnly, campaignData])

  return <>
    <form onSubmit={handleSubmit(onSubmit)} onKeyDown={(e) => checkKeyDown(e)} noValidate>
      <div class="text-center">
        <h2 id="create-title" class="text-3xl mt-3 mb-5">Create a campaign</h2>
      </div>
      {/* <!-- Title --> */}
      <FormGroup className="mt-6">
        <p className="text-2xl mb-2" ><label for="title">Title</label></p>
        <Input readonly={readOnly} type="text" error={errors.title} id="title" { ...register("title", { required: true }) }/>
        <ErrorMessages error={errors.title} messages={errorMessages.title}></ErrorMessages>
      </FormGroup>
      
      {/* <!-- Hero image/url --> */}
      <FormGroup className="mt-6">
        <p className="text-2xl mb-2" ><label for="image">Hero</label> <small class="text-xs">(url to image)</small></p>
        <Input type="text" readonly={readOnly} error={errors.image} id="title" { ...register("image", { validate: validateURL }) }/>
        <ErrorMessages error={errors.image} messages={errorMessages.image}></ErrorMessages>
      </FormGroup>

      {/* <!-- Markdown files --> */}
      <FormGroup className="mt-6">
        <p class="text-2xl mb-2">Description</p>
        <p>Provide your potential investors with in-depth information about your project. <a class="text-nowrap text-blue-500" data-toggle="modal" data-target="#modal-abstract-demo" href="#"><u>Learn more</u></a>
        </p>
        <div class="row mt-4">
          <div id="markdownEditors" class="col tab-content">
            <div class="tab-pane fade show active" id="english" role="tabpanel" aria-labelledby="english-tab">
              <div className={`${errors.description ? 'border border-red-500 rounded-lg' : ''}`}>
                <Label className="hidden" hidden for="abstractEN">Project Abstract <small>(Markdown Format Supported)</small></Label>
                <SimpleMDE className={`simple-mde ${readOnly ? "readonly" : ""}`} options={mdeOptions} getCodemirrorInstance={getCmInstanceCallback} rows="4" { ...descriptionProps } value={description} onChange={(val) => setValue('description', val, { shouldValidate: true, shouldTouch: true })} ></SimpleMDE>
              </div>
              <ErrorMessages error={errors.description} messages={errorMessages.description}></ErrorMessages>
            </div>
          </div>
        </div>
      </FormGroup>

      {/* <!-- Expires --> */}
      <FormGroup className="-mt-4 mb-8">
        <p className="text-2xl mb-2" ><label for="expires">End date</label>{/* <small class="text-xs">(UTC±00:00)</small> */}</p>
        <Input type="date" readonly={readOnly} min={todayFormatted} error={errors.expires} id="title" { ...register("expires") }/>
        <ErrorMessages error={errors.expires} messages={errorMessages.expires}></ErrorMessages>
      </FormGroup>

      {/* <!-- Recipients --> */}
      <FormGroup>
        <div id="recipients">
          <p class="text-2xl mb-2 pb-2 border-b">Recipients</p>
          { recipients.map((recipient, index) => {
            const recipientErrors = errors?.recipients?.[index] || {};

            return <div class="recipient mt-4" key={recipient.id}>
              <div class="flex justify-between">
                <p class="mb-2">Recipient {index + 1}</p>
                <div class="text-red-500 cursor-pointer" onClick={() => recipients.length === 1 ? updateRecipient(index, emptyRecipient) : removeRecipient(index)}>{ recipients.length === 1 ? "Clear" : "Remove" }</div>
              </div>
              <FormRow className="text-muted">
                <FormItem className="col-lg-3" error={getErrorMessage(recipientErrors.name, errorMessages.recipient.name)}>
                  <Label for="recipient_name[0]">Name</Label>
                  <Input readonly={readOnly} type="text" error={recipientErrors.name} id="recipient_name[0]"  { ...register(`recipients.${index}.name`, { required: true }) } />
                </FormItem>
                <FormItem className="col-lg-3" error={getErrorMessage(recipientErrors.bch, errorMessages.recipient.bch)}>
                  <Label for="amount[0]">Funding Goal <small>(BCH)</small></Label>
                  <Input readonly={readOnly} Component={CurrencyInput} error={recipientErrors.bch} value={getValues(`recipients.${index}.bch`)} inputmode="decimal" autocomplete="off" decimalScale={8} decimalsLimit={8} { ...register(`recipients.${index}.bch`, { required: true, validate: validateBch }) } onValueChange={(val) => setValue(`recipients.${index}.bch`, val, { shouldValidate: true, shouldTouch: true })}/>
                </FormItem>
                <FormItem className="col-lg-6" error={getErrorMessage(recipientErrors.address, errorMessages.recipient.address)}>
                  <Label for="bch_address[0]">Bitcoin Cash Address</Label>
                  <Input readonly={readOnly} type="text" error={recipientErrors.address} id="bch_address[0]" { ...register(`recipients.${index}.address`, { required: true, validate: validateBchAddress }) } />
                </FormItem>
              </FormRow>
              {/* <FormRow className="text-muted">
                <FormItem className="col-lg-6" error={recipientErrors.image?.type === 'validate' && "Invalid URL format."}>
                  <Label for="image_url[0]">Image URL</Label>
                  <Input readonly={readOnly} type="text" error={recipientErrors.image} id="image_url[0]"{ ...register(`recipients.${index}.image`, { validate: validateURL }) } />
                </FormItem>
                <FormItem className="col-md-6" error={recipientErrors.url?.type === 'validate' && "Invalid URL format."}>
                  <Label for="project_url[0]">Website</Label>
                  <Input readonly={readOnly} type="text" error={recipientErrors.url} id="project_url[0]" { ...register(`recipients.${index}.url`, { validate: validateURL }) }/>
                </FormItem>
              </FormRow> */}
            </div>
          }) }
        </div>
        <button id="add-recipient" class="text-blue-500 cursor-pointer" type="button" onClick={(e) => { e.preventDefault(); addRecipient(emptyRecipient)}}>Add a recipient</button>
      </FormGroup>
      
      <FormGroup className="mt-6 border-t">

        { showAdvanced ? <FormGroup className="mt-6">
          <p class="text-2xl mb-2">Advanced configuration</p>
          <p>Connect your campaign to the decentralized web. <a class="text-blue-500 text-nowrap" data-toggle="modal" data-target="#modal-abstract-demo" href="#"><u>Learn more</u></a>
          </p>
          {/* <!-- Set default gateway to show url --> */}
          
          <p class="mb-2 mt-4"><label for="default_gateway_url">Default IPFS Gateway</label></p>
          <FormRow className="mb-6">
            <FormItem>
              <Input type="text" class="form-control" id="default_gateway_url" { ...defaultGatewayProps } onBlur={_onDefaultGatewayBlur} defaultValue="https://dweb.link"/>
            </FormItem>
          </FormRow>

          <div class="mt-6">
            <div id="preload-nodes">
              <div class="preload-node">
                <p class="mb-1">Preload Nodes</p>
                  <div class="my-2 text-sm">
                    { preloadNodes.map((preloadNode, index) => { 
                      const preloadNodeErrors = errors?.preloadNodes?.[index] || {}

                      const preloadFailed = campaignData && campaignData.successfulPreloadNodeIndexes.indexOf(index) === -1;
                      const preloadSucceeded = campaignData && !preloadFailed

                      return <div key={preloadNode.id}>
                        <div class="flex justify-between sm:block">
                          <p class="my-4">Preload Node {index + 1}</p>
                          <div class="block sm:hidden text-red-500 cursor-pointer" onClick={() => preloadNodes.length === 1 ? updatePreloadNode(index, emptyPreloadNode) : removePreloadNode(index)}>Remove</div>
                        </div>
                        <FormRow gap={1}>
                          <FormItem error={preloadFailed && "Failed to upload"} success={preloadSucceeded && "Upload success"}>
                            <Label for="preload_node[${index}]">URL</Label>
                            <Input type="text" error={preloadNodeErrors.url} class="form-control check-url" id={`preload_url[${index}]`} { ...register(`preloadNodes.${index}.url`, { required: true, validate: validateURL  }) } />
                          </FormItem>
                          <FormItem class="form-group col-lg-6">
                            <Label for="preload_multiaddr[${index}]">Multiaddress</Label>
                            <Input type="text" class="form-control check-multiaddrs" id={`preload_multiaddr[${index}]`} { ...register(`preloadNodes.${index}.multiaddr`) }/>
                          </FormItem>
                          <div class="hidden sm:block text-red-500 cursor-pointer m-auto translate-y-1/2" onClick={() => preloadNodes.length === 1 ? updatePreloadNode(index, emptyPreloadNode) : removePreloadNode(index)}>{ preloadNodes.length === 1 ? "Clear" : "Remove" }</div>
                        </FormRow>
                        { preloadNodeErrors.url && preloadNodeErrors.url.type === 'required' ? <span class="text-red-500 text-sm">Preload URL is required</span> : <></>}
                        { preloadNodeErrors.url && preloadNodeErrors.url.type !== 'required' ? <span class="text-red-500 text-sm">Invalid URL format.</span> : <></>}
                      </div>
                    }) }
                    <button id="add-preload-node" class="mt-2 text-blue-500 cursor-pointer" type="button" onClick={() => addPreloadNode({ multiaddr: "", url: "" })}>Add a node</button>
                  </div>
              </div>
            </div>
          </div>
        </FormGroup> : <></> }
                  
        <FormGroup>
          <p class="py-6">Please <strong>disable your popup blocker</strong> immediately and check that everything is correct, you will not be able to edit your campaign later on. Leave this tab open for the duration of your campaign.</p>

          {/* <!-- Error message --> */}
          <p class="text-red-500 hidden" id="error">Some fields are incorrect.</p>

          {/* <!-- Submit button --> */}
          {/* <!-- <button id="preview" class="btn btn-secondary btn-lg btn-block">Preview</button> --> */}
          <div class="flex flex-wrap justify-between items-center">
            <button disabled={!ipfs} type="submit" id="create" class="flex-grow text-white px-8 py-4 rounded bg-green-600 hover:bg-green-700 tracking-wide font-semibold shadow-lg disabled:bg-gray-300">{ isPublishing ? "Loading..." : isSuccess ? "Success!" : "Create" }</button>
            <div id="advanced-config-link-container" class="flex-grow text-right">
              <a class="text-blue-500" href="#" onClick={(e) => { e.preventDefault(); setShowAdvanced(!showAdvanced) }}>{!showAdvanced ? "Show" : "Hide"} advanced configuration</a>
            </div>
          </div>

          <br />

          <p id="result" class={`cursor-pointer overflow-hidden overflow-ellipsis text-sm text-center text-green-400 ${!campaignData?.hash ? "hidden" : ""}`}>
              <a class="font-semibold text-green-500 underline" href={campaignData?.url} target="_blank">{campaignData?.url}</a>
              <br />
              Share or <a class="underline" href={campaignData?.download} download={`${campaignData?.hash}.car`}>download</a> this hash to keep it accessible on the web.
          </p>
        </FormGroup>
      </FormGroup>
    </form>
  </>
}

function getErrorMessage(error, messages) {
  const message = messages && error && error.type && messages[error.type];
  return typeof message === 'function' ? message(error) : message ;
}

const errorMessages = {
  title: {
    required: 'Fundraiser requires a title.'
  },
  image: {
    validate: "Invalid url."
  },
  description: { 
    required: "Fundraiser requires a description."
  },
  expires: {
    required: "Fundraiser requires an expiration date.",
  },
  recipient: {
    name: { 
      required: 'Name is required.',
    },
    bch: { 
      required: 'Amount is required.',
      validate: "Minimum amount is 546 SATS"
    },
    address: { 
      required: 'Address is required.',
      validate: "Invalid BCH address."
    }
  }
}



function ErrorMessages({ error, messages }) {

  const message = getErrorMessage(error, messages);

  return  message ? <span class="text-red-500 text-sm">{message}</span> : <></>
}

// Check if URL is valid
function validateURL(textval) {
  if (!textval) return true;
  if (process.env.NODE_ENV === 'development' && /localhost(:([1-9]){1,6})?/.test(textval)) return true

  // regex modified from https://github.com/jquery-validation/jquery-validation/blob/c1db10a34c0847c28a5bd30e3ee1117e137ca834/src/core.js#L1349
  var urlregex = /^(?:(?:(?:https?):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;
  return urlregex.test(textval);
}

function validateBchAddress(value) {
  if (!value) return true;
  return bchaddr.isValidAddress(value) && ! bchaddr.isLegacyAddress(value);
}

import { SATS_PER_BCH, MIN_SATOSHIS } from "../../../utils/bitcoinCashUtilities.js"

function bchToSats(bch) {
  const sats = Math.round(parseFloat(bch) * SATS_PER_BCH);
  return sats;
}

function validateBch(val) {
  const sats = bchToSats(val);
  return sats >= MIN_SATOSHIS;
}

export function Label({ ...props }) {
  return <label {...props} className={`block mb-2 text-gray-600 ${props.className || ""}`}>{ props.children }</label>
}

const _Input = forwardRef((props, ref) => <input {...props} ref={ref}></input>);
const _Select = forwardRef((props, ref) => <select {...props} ref={ref}></select>);

export const Input = forwardRef(({ error, Component, noPadding, ...props }, ref) => {
  const className = `block h-10 w-full border ${props.readonly ? 'bg-gray-100' : ''} ${error ? 'border-red-300' : ''} rounded-lg outline-none bg-white ${noPadding ? "" : "px-3 py-1.5"} cursor-auto text-gray-600 transition-shadow focus:shadow-[0_0_0_0.2rem_rgb(0,123,255,0.25)] focus:border-blue-200 ${props.className || ""}`
  
  let _Component;
  if (Component) {
    _Component = Component;
  } else {
    _Component = props.type === 'select' ? _Select : _Input
  }

  return <_Component {...props} className={className} ref={ref}>{ props.children }</_Component>
});

export function FormGroup({ ...props }) {
  const className = `${props.className || ""}`
  return <div {...props} className={className}>{props.children}</div>
}

export function FormRow({ gap = 4, mb = 2, ...props }) {
  const className = `flex flex-col sm:flex-row justify-between items-end gap-${gap} mb-${mb} ${props.className || ""}`
  return <div {...props} className={className}>{ props.children }</div>
}

export function FormItem({ error, success, ...props }) {
  const className = `w-full pb-3 ${props.className || ""}`
  return <div {...props} className={className}>
    { props.children }
    { error ? <span class="absolute text-red-500 text-sm">{ error }</span> : <></> }
    { error ? <span class="absolute text-green-500 text-sm">{ success }</span> : <></> }
  </div>
}

const UploadFileInput = styled.input.attrs({
  type: "file"
})`
  appearance: none;

  &::-webkit-file-upload-button{
    background:#1f2937;
    border:0;
    color:#fff;
    cursor:pointer;
    font-size:.875rem;
    font-weight:500;
    height: 100%
  }

  &::-webkit-file-upload-button:hover {
    background:#374151;
  }

  .dark &::-webkit-file-upload-button {
    background:#4b5563;
    color:#fff;
  }

  .dark &::-webkit-file-upload-button:hover {
    background:#6b7280
  }
`