import prettyPrintSats from "../../../utils/prettyPrintSats.js";
import useContributionsQuery from "../../hooks/useContributions.js";
import Modal from "../../modal.js";
import PrimaryButton from "../../primary-button.js"
import SecondaryButton from "../../secondary-button.js"
import { Link } from "preact-router";
import ContributionRow from "./contribution.js";

import { keys, get, del } from "idb-keyval";
import campaignStore from "../../../utils/campaignStore.js";

import { useState, useEffect, useMemo, useCallback } from "preact/hooks";

import useBroadcastTransaction from "../../hooks/useBroadcastTransaction.js";
import { createFlipstarterFullfillmentTransaction } from "@ipfs-flipstarter/utils/utils/flipstarterTransactions.js";
import { feesFor } from "@ipfs-flipstarter/utils/wallet/index.js";

import { useQueryClient } from 'react-query';
import { contributionQueryKeys } from '@ipfs-flipstarter/utils/queries/contributions.js';

import { memo } from "preact/compat";
import { MIN_SATOSHIS } from "../../../utils/bitcoinCashUtilities.js";

export const CampaignCard = memo(({ campaign, onOpen }) => {
  const contributionsQuery = useContributionsQuery(campaign?.recipients);
  const { contributions = [], isFullfilled = false, totalRaised:committedSatoshis = 0, fullfillmentFees = 0 } = contributionsQuery.data || {}

  const image = campaign.image || campaign?.recipients?.[0]?.image;

  const requestedSatoshis = (campaign?.recipients || []).reduce((sum, recipient) => {
    return sum + recipient.satoshis;
  }, 0);

  const [requestedAmountText, requestedAmountDenomination] = prettyPrintSats(requestedSatoshis);
  const [committedAmountText, committedAmountDenomination] = prettyPrintSats(committedSatoshis);
  const percentageCompleted = Math.round((committedSatoshis / requestedSatoshis) * 100);
  const readyToFullfill = requestedSatoshis + fullfillmentFees <= committedSatoshis;

  const pledgeCount = contributions.length;

  function onCardClick() {
    onOpen(campaign);
  }

  return <div class="cursor-pointer border rounded-3xl shadow-xl overflow-hidden" onClick={onCardClick}>
    <div>
      <div>
        <div>
          {/* Card Hero */}
          <div class="bg-gray-200 relative" style="padding-top:56%;">
            <div class="absolute w-full h-full inset-0 bg-cover bg-center" style={`background-image:url(${image})`}></div>
          </div>

          {/* Card Body */}
          <div class="p-4">
            <h4 class="pb-2 mb-2 border-b border-gray-400"><Link href={`/manage/${campaign.id}`}>{campaign.title}</Link></h4>
            <div class="text-sm font-light">
              <div class="my-2">
                <div class="mb-1 bg-gray-200 flex h-4 overflow-hidden rounded-xl">
                  <div role="progressbar" class="bg-green-400" style={`width: ${percentageCompleted || 0}%;`}></div>
                </div>
                <small class="mx-1 flex flex-row flex-wrap gap-x-2"><span>{committedAmountText} {committedAmountDenomination} of {requestedAmountText} {requestedAmountDenomination}</span> <span>({percentageCompleted}% completed)</span></small>
              </div>
              <div class="flex justify-end xs:justify-between">
                <div class="hidden xs:block">
                  <p class="uppercase">{isFullfilled ? "Fullfilled" : readyToFullfill ? "Fullfill Now" : "Ongoing"}</p>
                </div>
                <div class="text-right">
                  <p>{pledgeCount} { pledgeCount === 1 ? "Pledge" : "Pledges" }</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
})

export function CampaignModal({ campaign, onClose }) {
  const contributionsQuery = useContributionsQuery(campaign?.recipients);
  const { contributions:campaignContributions, isFullfilled:campaignIsFullfilled, totalRaised:committedSatoshis, fullfillmentFees } = contributionsQuery.data || {};
  const [pickedContributions, setPickedContributions] = useState([]);
  const [pickedContributionsDirty, setPickedContributionsDirty] = useState(false);

  const pickedSatoshis = (pickedContributions || []).reduce((sum, contribution) => {
    return sum + contribution.satoshis;
  }, 0);

  const requestedSatoshis = (campaign?.recipients || []).reduce((sum, recipient) => {
    return sum + recipient.satoshis;
  }, 0);

  //TODO God willing: totalRaised isn't always the amount of contributions total.
  let minimumNeeded = 0;
  const goal = requestedSatoshis + fullfillmentFees;
  if (!campaignIsFullfilled && committedSatoshis < goal) {
    const remainingAmount = goal - committedSatoshis;
    minimumNeeded = remainingAmount < MIN_SATOSHIS ? MIN_SATOSHIS : remainingAmount
  }
  
  const minerFee = feesFor(pickedContributions.length, campaign?.recipients?.length || 0);

  const [requestedAmountText, requestedAmountDenomination] = prettyPrintSats(requestedSatoshis);
  const [committedAmountText, committedAmountDenomination] = prettyPrintSats(committedSatoshis);
  const [neededAmountText, neededAmountDenomination] = prettyPrintSats(minimumNeeded);
  const [feeAmountText, feeAmountDenomination] = prettyPrintSats(minerFee);

  const showFullfillment = !campaignIsFullfilled && minimumNeeded === 0;

  useEffect(() => {
    if (pickedContributionsDirty) return;
    if (!campaignContributions?.length) return;

    let total = 0;
    const pickedContributions = [];

    for (let i = 0; i < campaignContributions.length; i++) {
      const minerFee = feesFor(pickedContributions.length, campaign?.recipients?.length || 0);
      if (total >= requestedSatoshis + minerFee) break;

      const contribution = campaignContributions[i];
      if (contribution.fullfillment) continue;
      
      total += contribution.satoshis;
      pickedContributions.push(contribution);
    }

    setPickedContributions(pickedContributions);
  }, [campaignContributions, requestedSatoshis, campaign?.recipients?.length || 0]);

  function doPickContribution(contribution) {

    let updated = pickedContributions.indexOf(contribution) !== -1 ?
      //Remove
      pickedContributions.filter(c => c !== contribution) :
      //Add
      [...pickedContributions, contribution];

    setPickedContributionsDirty(true);
    setPickedContributions(updated);
  }

  const [isFullfilling, setIsFullfilling] = useState(false);
  const [fullfillmentSuccess, setFullfillmentSuccess] = useState(false);
  const [fullfillmentError, showFullfillmentError] = useState(null);

  const queryClient = useQueryClient();
  const broadcastTransaction = useBroadcastTransaction();

  async function startFullfillment() {
    setIsFullfilling(true);
    showFullfillmentError(null);
    
    try {

      if (pickedSatoshis > requestedSatoshis) {
        const fullfillmentTransaction = await createFlipstarterFullfillmentTransaction(campaign.recipients, pickedContributions);
        const fullfillmentHash = await broadcastTransaction(fullfillmentTransaction);
        console.log(fullfillmentHash);
        
        await queryClient.invalidateQueries(contributionQueryKeys.list(campaign.recipients));
        setIsFullfilling(false);
        setFullfillmentSuccess(true);

      } else {
        showFullfillmentError("The total is less than the fundraiser goal!");
      }
    } catch(err) {
      console.log(err);
      showFullfillmentError("Unknown error occured.");
    } finally {
      setIsFullfilling(false);
    }
  }

  const [showDelete, setShowDelete] = useState(false);
  function onStartDelete() {
    setShowDelete(true);
  }

  function onQuitDelete() {
    setShowDelete(false)
  }

  async function onDelete() {
    await del(campaign.id, campaignStore);
    onClose(true);
  }

  useEffect(() => {
    if (fullfillmentSuccess) {
      const timeout = setTimeout(() => {
        setFullfillmentSuccess(false);
      }, 2000)

      return () => clearTimeout(timeout);
    }
  }, [fullfillmentSuccess])

  return <>
    { !showDelete ? 
      <Modal
        heading={<>Contributions to <span class="font-medium pr-1">{campaign.title}</span> fundraiser</>}
        footer={<div class="flex justify-between gap-4 flex-wrap">
          { showFullfillment ? <PrimaryButton disabled={pickedSatoshis < requestedSatoshis + minerFee} onClick={startFullfillment}>{ isFullfilling ? "Fullfilling..." : fullfillmentSuccess ? "Done!" : "Start fullfillment" }</PrimaryButton> : <></>}
          <SecondaryButton onClick={onClose}>Done</SecondaryButton>
          <SecondaryButton className="!text-red-700 xs:ml-auto" onClick={onStartDelete}>Delete</SecondaryButton>          
        </div>}
      >
        <div class="overflow-x-auto w-full">
        { 
          !campaignContributions.length ? 
            <div class="text-center text-gray-600 text-lg py-6 font-thin">No contributions found for this fundraiser.</div> :
            <>
              <table class="table w-full">
                {/* <!-- head --> */}
                <thead>
                  <tr>
                    { showFullfillment ? <th class="text-center pr-2">Select</th> : <></> }
                    <th>Transaction</th>
                    <th>State</th>
                    <th>Amount</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  { campaignContributions.map((contribution, index) => {
                      const id = contribution.txHash + ":" + contribution.txIndex;
                      const shouldUse = pickedContributions.indexOf(contribution) !== -1;

                      return <ContributionRow key={id}
                        contribution={contribution}
                        showSelect={showFullfillment && !contribution.fullfillment} 
                        selected={!!shouldUse}
                        onToggle={() => doPickContribution(contribution)}
                      ></ContributionRow>
                    }) 
                  }
                </tbody>    
              </table>
              <div class="text-sm flex flex-col gap-2 pt-4 border-t mt-4">
                  <div>Total Requesting: { requestedAmountText } { requestedAmountDenomination }</div>
                  <div>Total Raised: { committedAmountText } { committedAmountDenomination }</div>
                  { showFullfillment ? <div>Total Fees: { feeAmountText } { feeAmountDenomination }</div> : <></> }
                  { minimumNeeded !== 0 ? <div>Minimum to complete: { neededAmountText } { neededAmountDenomination }</div> : <></> }
              </div>
              { fullfillmentError ? <p class="text-sm text-red-500">{ fullfillmentError }</p>: <></> }
            </>
        }
        </div>
      </Modal> : 
      <Modal
        heading={<div class="font-normal">Are you sure you'd like to delete the <span class="font-bold pr-1">{campaign.title}</span> fundraiser?</div>}
        footer={<div class="flex justify-end gap-4">
          <SecondaryButton className="!text-red-700" onClick={onDelete}>Delete</SecondaryButton>
          <SecondaryButton onClick={onQuitDelete}>Back</SecondaryButton>
        </div>}
      >
      </Modal>
    }
  </>
}
import { useIpfs } from "../../withIpfs.js";

export default function ManagePage({ setIsLoading }) {
  const { ipfs } = useIpfs();
  const [campaigns, setCampaigns] = useState([]);

  useEffect(async () => {
    setIsLoading(true);

    const campaignIds = await keys(campaignStore);
    const campaigns = await Promise.all(campaignIds.map(async (id) => {
      const campaignData = await get(id, campaignStore);
      return campaignData;
    }));

    setCampaigns(campaigns);
    setIsLoading(false);
  }, []);

  const [showCampaign, setShowCampaign] = useState(null); 

  useEffect(() => {
    if (ipfs && showCampaign) {
      ipfs.connectToPreloadNodes(showCampaign.preloadNodes);
    }
  }, [ipfs, showCampaign]);

  const openContribtionsModal = useCallback((campaign) => {
    setShowCampaign(campaign);
  }, []);

  const closeContributionModal = useCallback(async (refetch) => {
    setShowCampaign(null);

    if (refetch === true) {
      const campaignIds = await keys(campaignStore);
      const campaigns = await Promise.all(campaignIds.map(async (id) => {
        const campaignData = await get(id, campaignStore);
        return campaignData;
      }));
  
      setCampaigns(campaigns);
    }
  }, []);

  return <div id="listCampaigns">
    <div id="campaigns" class="">
      { !campaigns.length ? <div class="absolute inset-0 flex justify-center items-center opacity-50">
        <div>
          <div class="text-center">
              <div class="mt-3">
                  <h2 class="text-3xl mb-2">No campaigns</h2>
                  <em><a class="text-blue-500 cursor-pointer" href="#/">Create one here!</a></em>
              </div>
          </div>
        </div>
      </div> : <div>
          <div class="mb-8">
              <div class="text-center">
                  <div class="mt-3 mb-5">
                      <h2 class="text-3xl mb-2">Created campaigns</h2>
                  </div>
              </div>
          </div>
          <div class="grid sm:grid-cols-2 lg:grid-cols-3 gap-8">
            {/* <!--Items go here --> */}
            { campaigns.map((campaign) => {
                return <CampaignCard key={campaign.id} campaign={campaign} onOpen={openContribtionsModal}></CampaignCard>
            })}
          </div>
      </div> }


    {
      showCampaign ? <CampaignModal campaign={showCampaign} onClose={closeContributionModal}></CampaignModal> : <></>
    }
    </div>
  </div>
}