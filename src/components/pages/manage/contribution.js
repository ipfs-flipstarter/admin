import prettyPrintSats from "../../../utils/prettyPrintSats.js";
import { contributionQueryKeys } from '@ipfs-flipstarter/utils/queries/contributions.js';
import { useQuery } from 'react-query';
import { useIpfs } from "../../withIpfs.js"
import { Transaction } from "@bitcoin-dot-com/bitcoincashjs2-lib"
import moment from "moment";
import { useMemo } from "preact/hooks"
import useBroadcastTransaction from "../../hooks/useBroadcastTransaction.js";

export default function ContributionRow({ contribution, showSelect, selected, onToggle, ...props }) {
  const [amountText, amountDenomination] = prettyPrintSats(contribution.satoshis);
  const { ipfs } = useIpfs();
  const { data: contributionData } = useQuery(contributionQueryKeys.fetchApplicationData(contribution), {
    staleTime: 30 * 1000,
    enabled: !!ipfs
  }) || {};

  const broadcastTransaction = useBroadcastTransaction();

  const refundValid = useMemo(() => {
    if (!contributionData?.refundTx) return;
    const tx = Transaction.fromHex(contributionData?.refundTx);
    const locktime = tx.locktime;
    if (locktime > 500000000) {
      return moment.unix(locktime) < moment();
    }
  }, [contributionData?.refundTx]);

  return <tr >
    { showSelect ? <th class="pr-2 text-center">
      <label>
        <input type="checkbox" class="checkbox" checked={!!selected} onChange={onToggle} />
      </label>
    </th> : <></> }
    <td class="pr-2">
      <div class="text-sm"><a class="text-blue-500" target="_blank" href={`https://blockchair.com/bitcoin-cash/transaction/${contribution.txHash}`} alt={contribution.txHash}>{ contribution.txHash.slice(0, 12) }:{ contribution.txIndex }</a></div>
    </td>
    <td class="pr-2">
      { contribution.fullfillment ? 
          <span class="bg-green-300 p-1 rounded text-white text-sm">Fullfillment</span> : 
          <span class="bg-gray-300 p-1 rounded text-white text-sm">Verified</span>
      }
    </td>
    <td class="pr-2">{ amountText } { amountDenomination }</td>
    <th>
      { refundValid && <button class="bg-gray-300 hover:bg-gray-500 p-1 rounded text-black text-opacity-60 hover:text-opacity-100 text-sm" onClick={() => broadcastTransaction(contributionData.refundTx)}>refund</button> }
    </th>
  </tr>
}