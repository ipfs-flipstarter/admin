import {render} from "react"
import App from "./components/app.js"

async function init() {
  if (process.env.NODE_ENV === "development") {
    await import('preact/devtools');
  }

  await render(<App></App>, document.getElementById("app"));
}

init();