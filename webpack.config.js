const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const IpfsCarWebpackPlugin = require('ipfs-car-webpack-plugin');
const NodePolyfillWebpackPlugin = require('node-polyfill-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');

require('dotenv').config({ path: `.env${process.env.NODE_ENV === "development" ? ".dev" : ""}` })

const jsCdnFiles = [
  require.resolve("./public/js/ipfs.min.js"),
  require.resolve("./public/js/ipfs-provider.min.js"),
  require.resolve("./public/js/ipfs-http-client.min.js"),
  require.resolve("./public/js/easymde.min.js"),
]

module.exports = {
  entry: {
    create: path.join(__dirname, "./src/index.js"),
  },
  output: {
    path: path.join(__dirname, "./dist"),
    filename: "static/js/[name].js",
    assetModuleFilename: 'static/media/[name][ext]'
  },
  externals: {
    ipfs: 'Ipfs',
    "ipfs-provider": "IpfsProvider",
    "ipfs-http-client": "IpfsHttpClient",
    easymde: 'EasyMDE',
  },
  resolve: {
    alias: {
      "react": "preact/compat",
      "react-dom/test-utils": "preact/test-utils",
      "react-dom": "preact/compat",     // Must be below test-utils
      "react/jsx-runtime": "preact/jsx-runtime"
    },
    fallback: {
      net: false,
      tls: false
    }
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          keep_fnames: true,
          safari10: true,
          keep_classnames: true,
          mangle: {
            reserved: [
              'ecurve'
            ]
          }
        }
      }),
    ],
  },
  module: {
    rules: [
      { 
        test: /\.html$/, 
        use: {
          loader: "html-loader",  
          options: {
            esModule: false
          }
        },
      },
      {
        test: /\.css$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "static/css/[name].[ext]",
            },
          },
          { 
            loader: "extract-loader", 
            options: {
              publicPath: "../../" //important for relative path to "/static" folder (where file-loader places css compared to other files)
            }
          },
          {
            loader: "css-loader",
            options: {
              esModule: false, //important for extract-loader
            }
          },
          {
            loader: "postcss-loader",
          }
        ],
      },
      {
        test: /\.(png|jpe?g|gif|woff2|woff|mp3|car|svg)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "static/media/[name].[ext]",
              publicPath: ""
            },
          },
        ],
      },
      {
        include: [...jsCdnFiles],
        use: [
          {
            loader: "file-loader",
            options: {
              name: "static/js/[name].[ext]",
              publicPath: ""
            },
          },
        ],
      },
      {
        test: /\.m?js$/,
        exclude: [
          /node_modules/, 
          ...jsCdnFiles
        ],
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              [
                "@babel/preset-env",
                {
                  targets: {
                    esmodules: true,
                  },
                },
              ],
            ],
            plugins: [
              ["babel-plugin-transform-jsx-to-htm", {
                "import": {
                  // the module to import:
                  "module": "htm/preact",
                  // a named export to use from that module:
                  "export": "html"
                }
              }]
            ]
          },
        },
      }
    ],
  },
  plugins: [
    new webpack.IgnorePlugin({
      resourceRegExp: /^\.\/wordlists\/(?!english)/, 
      contextRegExp: /bip39\/src$/
    }),
    new IpfsCarWebpackPlugin({
      filename: "static/client.car",
      path: path.resolve("./assets/client"),
      pattern: undefined,
      chunks: ["create"]
    }),
    // new BundleAnalyzerPlugin(),
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, "./views/create.html"),
      chunks: ["create"],
      filename: "index.html",
      favicon: path.join(__dirname, "./public/img/logo.ico"),
      inject: 'body'
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /^\.\/locale$/,
      contextRegExp: /moment$/,
    }),
    new webpack.DefinePlugin({

    }),
    new webpack.EnvironmentPlugin({
      DEFAULT_GATEWAY_URL: process.env.DEFAULT_GATEWAY_URL || "https://flipstarter.me",
      DEFAULT_API_URL: process.env.DEFAULT_API_URL, 
      DEFAULT_API_MULTIADDR: process.env.DEFAULT_API_MULTIADDR,
      PRELOAD_NODES: [
        { url: "https://node0.preload.ipfs.io" },
        { url: "https://node1.preload.ipfs.io" },
        { url: "https://node2.preload.ipfs.io" },
        { url: "https://node3.preload.ipfs.io" },
        ...([{ 
          url: process.env.DEFAULT_API_URL, 
          multiaddr: process.env.DEFAULT_API_MULTIADDR
        }] || []),
      ],
      ELECTRUM_SERVERS: [
        { address: "bch.imaginary.cash", scheme: "wss" },
        { address: "electroncash.dk", scheme: "wss" },
        { address: "electrum.imaginary.cash", scheme: "wss" },
      ],
      FLIPSTARTER_CLIENT: process.env.FLIPSTARTER_CLIENT,
      FLIPSTARTER_CLIENT_IPNS: process.env.FLIPSTARTER_CLIENT_IPNS,
      FLIPSTARTER_CLIENT_RECORD: process.env.FLIPSTARTER_CLIENT_RECORD,
    }),
    new NodePolyfillWebpackPlugin()
  ],
}
